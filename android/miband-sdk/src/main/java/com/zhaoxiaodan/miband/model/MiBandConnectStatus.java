package com.zhaoxiaodan.miband.model;

/**
 * Created by nagilin on 15/09/23.
 */
public enum MiBandConnectStatus {
    CONNECTED,
    DISCONNECTED,
    CONNECTING
}
