package tw.nagi.app.blehealth.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.NotifyListener;
import com.zhaoxiaodan.miband.RealtimeStepsNotifyListener;
import com.zhaoxiaodan.miband.model.UserInfo;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import tw.nagi.app.blehealth.R;
import tw.nagi.app.blehealth.cview.CircleProgressBar;
import tw.nagi.app.blehealth.util.SharedPreferencesInterface;
import tw.nagi.app.blehealth.util.StringUtils;

/**
 * Created by nagilin on 15/09/23.
 */
public class MiBandActivity extends AppCompatActivity implements SharedPreferencesInterface, ActionBar.OnNavigationListener{
    public static final String TAG = MiBandActivity.class.getSimpleName();
    private View vFinding, vDismiss, vMain, vSleep;
    private CircleProgressBar vProgressBar;
    private static final int SET_USERINFO      = 1;
    private static final int SET_PAIR          = 2;
    private static final int SET_NOTIFICATION  = 3;
    private static final int SET_NOTI_REALSTEP = 4;
    private static final int GET_BATTERY       = 5;
    private static final int SET_ACTIVITY_MAIN = 6;
    private static final int GET_REALSTEP      = 7;



    private boolean mUploadEnable = false;
    private SpinnerAdapter mSpinnerAdapter;
    private ActionHandler mHandler;
    private MiBand miband;

    public ActionHandler getHandler() {
        return this.mHandler;
    }

    private SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //UI setting
        vFinding = findViewById(R.id.main_layout_finding);
        vDismiss = findViewById(R.id.main_layout_dismiss);
        vMain = findViewById(R.id.main_layout_main);
        vSleep = findViewById(R.id.main_layout_sleep);
        mHandler = new ActionHandler();

        vProgressBar = (CircleProgressBar)findViewById(R.id.main_progressbar);
        sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        mSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.spin_list, R.layout.spinner_dropdown_item);
        getSupportActionBar().setListNavigationCallbacks(mSpinnerAdapter, this);





        miband = new MiBand(this);
        miband.setReconnect(true);
        miband.connect(new ActionCallback() {
            @Override
            public void onSuccess(Object o) {
                Log.i(TAG, "[+] Connect device success.");
                mHandler.sendEmptyMessage(SET_USERINFO);
                mHandler.sendMessageDelayed(getMessage(SET_NOTIFICATION), 1000);
                mHandler.sendMessageDelayed(getMessage(SET_NOTI_REALSTEP), 3000);
                mHandler.sendMessageDelayed(getMessage(SET_ACTIVITY_MAIN), 5000);
            }

            @Override
            public void onFail(int i, String s) {
                Log.e(TAG, "[-] Connect device fail.");
                vFinding.setVisibility(View.GONE);
                fade(vDismiss, View.VISIBLE);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    Log.d(TAG, "upload? (T/F): " + mUploadEnable);
                    try {
                        Thread.sleep(60000);
                        if(!mUploadEnable)
                            continue;
                        upload();
                    }catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();  //start upload after 15 sec
    }

    private Message getMessage(int what) {
        Message msg = new Message();
        msg.what = what;
        return msg;
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        switch(itemPosition) {
            case 0: //realstep
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vSleep.setVisibility(View.GONE);
                        fade(vMain, View.VISIBLE);
                    }
                });
                break;
            case 1:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vMain.setVisibility(View.GONE);
                        fade(vSleep, View.VISIBLE);
                    }
                });
                break;
        }
        return false;
    }


    private class ActionHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case SET_USERINFO:
                    Log.i(TAG, "[+] setting userinfo.");
                    UserInfo userinfo = new UserInfo(sp.getInt(KEY_ID, 1), 1, 32, 180, 55, "testuser", 1);
                    miband.setUserInfo(userinfo);
                    break;
                case SET_PAIR:
                    Log.i(TAG, "[+] setting pair.");

                    break;
                case SET_NOTIFICATION:
                    Log.i(TAG, "[+] setting normal notification.");
                    miband.setNormalNotifyListener(new NotifyListener() {
                        @Override
                        public void onNotify(byte[] bytes) {
                            Log.i(TAG, "[+] NormalNotify : " + StringUtils.bytesToHex(bytes));
                        }
                    });
                    break;
                case SET_NOTI_REALSTEP:
                    Log.i(TAG, "[+] setting step notification.");
                    miband.setRealtimeStepsNotifyListener(new RealtimeStepsNotifyListener() {
                        @Override
                        public void onNotify(int i) {
                            Log.i(TAG, "[+] RealtimeStepsNotify : " + i);
                            vProgressBar.setProgressNotInUiThread(i);
                            if(vProgressBar.getMaxProgress() - i < 10) {
                                vProgressBar.setMaxProgress(vProgressBar.getMaxProgress() * 5);
                            }

                        }
                    });
                    miband.enableRealtimeStepsNotify();
                    break;
                case GET_BATTERY:
                    break;
                case SET_ACTIVITY_MAIN:
                    vFinding.setVisibility(View.GONE);
                    vDismiss.setVisibility(View.GONE);
                    vSleep.setVisibility(View.GONE);
                    fade(vMain, View.VISIBLE);
                    break;
                case GET_REALSTEP:
                    miband.getRealstep(new ActionCallback() {
                        @Override
                        public void onSuccess(Object data) {
                            int i = (int)data;
                            Log.i(TAG, "[+] getRealstep : " + i);
                            vProgressBar.setProgressNotInUiThread(i);
                            if(vProgressBar.getMaxProgress() - i < 10) {
                                vProgressBar.setMaxProgress(vProgressBar.getMaxProgress() * 5);
                            }
                        }

                        @Override
                        public void onFail(int errorCode, String msg) {

                        }
                    });
                    break;
            }
        }
    };

    public void fade(final View view, final int viewStatus) {
        AlphaAnimation anime = new AlphaAnimation(((viewStatus == View.GONE) ? 1f : 0f), (viewStatus == View.GONE) ? 0f : 1f);
        anime.setDuration(500);
        view.startAnimation(anime);
        view.setVisibility(viewStatus);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_miband, menu);
        mUploadEnable = sp.getBoolean(KEY_UPLOADING, false);
        menu.findItem(R.id.action_upload).setIcon( mUploadEnable ? R.drawable.ic_upload : R.drawable.ic_upload_disable);
        menu.findItem(R.id.action_upload).setTitle("Upload " + (mUploadEnable ? "Enable" : "Disable") );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_history:
                startActivity(new Intent(MiBandActivity.this, HistoryActivity.class));
                break;
            case R.id.action_refresh:
                miband.connect(new ActionCallback() {
                    @Override
                    public void onSuccess(Object o) {
                        Log.i(TAG, "[+] Connect device success.");
                        mHandler.sendEmptyMessage(SET_USERINFO);
                        mHandler.sendEmptyMessage(GET_REALSTEP);
                        mHandler.sendMessageDelayed(getMessage(SET_NOTIFICATION), 1000);
                        mHandler.sendMessageDelayed(getMessage(SET_NOTI_REALSTEP), 3000);
                        mHandler.sendMessageDelayed(getMessage(SET_ACTIVITY_MAIN), 5000);
                    }

                    @Override
                    public void onFail(int i, String s) {
                        Log.e(TAG, "[-] Connect device fail.");
                        vFinding.setVisibility(View.GONE);
                        fade(vDismiss, View.VISIBLE);
                    }
                });
                break;
            case R.id.action_upload:
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        upload();
//                    }
//                }).start();
                if(mUploadEnable) {
                    mUploadEnable = false;
                    item.setIcon(R.drawable.ic_upload_disable);
                    item.setTitle("Upload Disable");
                } else {
                    mUploadEnable = true;
                    item.setIcon(R.drawable.ic_upload);
                    item.setTitle("Upload Enable");
                }
                sp.edit().putBoolean(KEY_UPLOADING, mUploadEnable).commit();


                break;
            case R.id.action_logout:
                sp.edit().remove(KEY_ID).commit();
                startActivity(new Intent(MiBandActivity.this, WelcomeActivity.class));
                finish();
                break;
//            case R.id.action_reset:
//                miband.doFactoryReset(new ActionCallback() {
//                    @Override
//                    public void onSuccess(Object data) {
//                        Log.i(TAG, "Reset success");
//                    }
//
//                    @Override
//                    public void onFail(int errorCode, String msg) {
//                        Log.i(TAG, "Reset fail: " + msg);
//                    }
//                });
//                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void upload() {

        int now = ((CircleProgressBar)findViewById(R.id.main_progressbar)).getProgress();
        String urlParameters = "uid=" + 1 + "&ca=step&st=" + now;
        String targetURL = getString(R.string.ADDR_SERV) + getString(R.string.ADDR_INSERT) + "?" + urlParameters;
        HttpURLConnection conn= null;
        try {
            conn = (HttpURLConnection)new URL(targetURL).openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            String str = s.hasNext() ? s.next() : "";
            Log.i(TAG, str);
        } catch(Exception e) {
            e.printStackTrace();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MiBandActivity.this, "自動上傳成功", Toast.LENGTH_SHORT).show();
            }
        });

//        URL url;
//        HttpURLConnection connection = null;
//        try {
//            //Create connection
//            url = new URL(targetURL);
//            connection = (HttpURLConnection)url.openConnection();
//            connection.setRequestMethod("POST");
//            connection.setRequestProperty("Content-Type",
//                    "application/x-www-form-urlencoded");
//
//            connection.setRequestProperty("Content-Length", "" +
//                    Integer.toString(urlParameters.getBytes().length));
//            connection.setRequestProperty("Content-Language", "en-US");
//
//            connection.setUseCaches (false);
//            connection.setDoInput(true);
//            connection.setDoOutput(true);
//
//            //Send request
//            DataOutputStream wr = new DataOutputStream (
//                    connection.getOutputStream ());
//            wr.writeBytes (urlParameters);
//            wr.flush ();
//            wr.close ();
//
//            //Get Response
//            InputStream is = connection.getInputStream();
//            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//            String line;
//            StringBuffer response = new StringBuffer();
//            while((line = rd.readLine()) != null) {
//                response.append(line);
//                response.append('\r');
//            }
//            rd.close();
//            Log.i(TAG, response.toString());
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        } finally {
//
//            if(connection != null) {
//                connection.disconnect();
//            }
//        }

    }

    private ActionBar.OnNavigationListener mOnNavigationListener = new ActionBar.OnNavigationListener() {
        @Override
        public boolean onNavigationItemSelected(int itemPosition, long itemId) {
            Log.i(TAG, ""+itemPosition);
            return true;
        }
    };
}
