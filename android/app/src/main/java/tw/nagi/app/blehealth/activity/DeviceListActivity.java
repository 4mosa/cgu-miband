package tw.nagi.app.blehealth.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tw.nagi.app.blehealth.R;
import tw.nagi.app.blehealth.util.SharedPreferencesInterface;

/**
 * Created by nagilin on 15/08/31.
 */
public class DeviceListActivity extends AppCompatActivity implements SharedPreferencesInterface {
    public static final String TAG = DeviceListActivity.class.getSimpleName();

    private BluetoothAdapter        mBluetoothAdapter;
    private BluetoothManager        mBluetoothManager;
    private LinearLayout            mLinearLayout;
    private Handler             mHandler;
    private boolean             mScanning;
    private String                mDeviceAddr;
    private List<BluetoothDevice> mListDevice;
    private static final int    BLUETOOTH_DELAY = 10000;    //10 sec

    private BleListAdapter mListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_layout);
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            //do not have bt_le
            Toast.makeText(this, getString(R.string.DIALOG_MSG_NO_BLE), Toast.LENGTH_LONG).show();
            finish();
        }
        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        mHandler = new Handler();

        mDeviceAddr = (getIntent().hasExtra(KEY_DEVICE_ADDR)) ? getIntent().getStringExtra(KEY_DEVICE_ADDR) : "";
        mListAdapter = new BleListAdapter(this, android.R.layout.simple_list_item_single_choice, new ArrayList<BluetoothDevice>());
        setListAdapter(mListAdapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent();
                i.putExtra(KEY_DEV_ACTIVITY, mListAdapter.getItem(position));
                DeviceListActivity.this.setResult(REQ_OK, i);

                SharedPreferences sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);
                sp.edit().putString(KEY_DEVICE_ADDR, mListAdapter.getItem(position).getAddress()).commit();

//                Log.e(TAG, mListAdapter.getItem(position).getAddress());
                finish();
            }
        });

        mLinearLayout = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        scanLeDevice(true);
    }

    private void setListAdapter(ListAdapter adapter) {
        ((ListView)findViewById(android.R.id.list)).setAdapter(adapter);
    }

    private ListView getListView() {
        return (ListView) findViewById(android.R.id.list);
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Find device!");
                    mListAdapter.add(device);
                    mListAdapter.notifyDataSetChanged();
                    mLinearLayout.setVisibility(View.GONE);
                }
            });
        }
    };

    public void scanLeDevice(final boolean enable) {
        if(enable) {
            //設定BLUETOOTH_DELAY毫秒後停止搜尋
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);

                    mLinearLayout.setVisibility(View.GONE);
                    if (mListAdapter.getCount() == 0) {
                        findViewById(R.id.scan_cantfind).setVisibility(View.VISIBLE);
                    }

                }
            }, BLUETOOTH_DELAY);
            mLinearLayout.setVisibility(View.VISIBLE);
            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }



    private class BleListAdapter extends ArrayAdapter<BluetoothDevice> {

        int mResource;
        public BleListAdapter(Context context, int resource, List<BluetoothDevice> list) {
            super(context, resource, list);
            mResource = resource;
            mListDevice = list;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            BluetoothDevice device = getItem(position);
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(mResource, parent, false);
            }
            CheckedTextView ctv = (CheckedTextView) convertView.findViewById(android.R.id.text1);
            ctv.setText(String.format("%s (%s)", device.getName(), device.getAddress()));
            if(mDeviceAddr.equals(device.getAddress()))
                ctv.setChecked(true);
            return convertView;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case R.id.action_refresh:
                scanLeDevice(true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
