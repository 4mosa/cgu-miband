package tw.nagi.app.blehealth.service;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import tw.nagi.app.blehealth.util.MiBandConstants;
import tw.nagi.app.blehealth.util.SharedPreferencesInterface;

/**
 * Created by nagilin on 15/08/30.
 */
public class BluetoothLeService extends Service implements SharedPreferencesInterface {

    private final static String TAG = BluetoothLeService.class.getSimpleName();

    private BluetoothDevice mDevice;
    private List<BluetoothGattService> listGattService;
    private BluetoothGattCharacteristic mCharacteristic;
    private BluetoothGatt mBluetoothGatt;
    private final IBinder mBinder = new LocalBinder();
    private boolean autoReconnect = false;
    private int mConnectionState = STATE_DISCONNECTED;
    private boolean isLoop = false;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECT = "tw.nagi.app.blehealth.service.action_ble_gatt_connect";
    public final static String ACTION_GATT_DISCONNECT = "tw.nagi.app.blehealth.service.action_ble_gatt_disconnect";
    public final static String ACTION_GATT_SERVICE_DISCOVERED = "tw.nagi.app.blehealth.service.action_ble_gatt_service_discovered";
    public final static String ACTION_GATT_DATA_AVALIABLE = "tw.nagi.app.blehealth.service.action_ble_gatt_data_avaliable";
    public final static String ACTION_GATT_DATA_NOTIFICATE = "tw.nagi.app.blehealth.service.action_ble_gatt_data_changed";
    public final static String ACTION_GATT_WRITE_RESPONSE = "tw.nagi.app.blehealth.service.action_ble_gatt_write_response";
    public final static String ACTION_GATT_REALSTEP = "tw.nagi.app.blehealth.service.action_ble_gatt_realstep";


    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(ACTION_GATT_CONNECT);
                discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                mConnectionState = STATE_DISCONNECTED;
                broadcastUpdate(ACTION_GATT_DISCONNECT);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                buildServiceList(gatt);
                broadcastUpdate(ACTION_GATT_SERVICE_DISCOVERED);
            } else {
                Log.w(TAG, "[!] onServicesDiscovered not success:" + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if(characteristic.getUuid().equals(MiBandConstants.UUID_CHARACTERISTIC_REALTIME_STEPS)) {
                    //step
                }

                broadcastUpdate(ACTION_GATT_DATA_AVALIABLE, characteristic);
            } else {
                Log.w(TAG, "[!] onCharacteristicRead not success:" + status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(ACTION_GATT_DATA_NOTIFICATE, characteristic);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_WRITE_RESPONSE, characteristic);
            } else {
                Log.w(TAG, "[!] onCharacteristicRead not success:" + status);
            }
        }

    };


    public void receiveData(boolean loop, long delay) throws InterruptedException {
        this.isLoop = loop;
        setNotify();
        setRealtimeStepNotify();
        do {
            readRealtimeStep();
            Thread.sleep(delay);
        } while(this.isLoop);

        Log.i(TAG, "[ ] stop realtime step rec.");
    }


    public void connect(BluetoothDevice device) {
        Log.i(TAG, "[ ] Connect to gatt");
        mDevice = device;
        mBluetoothGatt = device.connectGatt(this, autoReconnect, mGattCallback);
    }


    public void discoverServices() {
        if(listGattService == null)
            mBluetoothGatt.discoverServices();
    }
//    public void test() {
//
//
//        mCharacteristic = getGattCharacteristicFromList(MiBandConstants.UUID_CHARACTERISTIC_CONTROL_POINT);
//        mCharacteristic.setValue(new byte[] {3, 1});
//        mBluetoothGatt.writeCharacteristic(mCharacteristic);
//
//
//    }

    public void setNotify() {
        mCharacteristic = getGattCharacteristicFromList(MiBandConstants.UUID_CHARACTERISTIC_NOTIFICATION);
        mBluetoothGatt.setCharacteristicNotification(mCharacteristic, true);
        BluetoothGattDescriptor descriptor = mCharacteristic.getDescriptor(MiBandConstants.UUID_DESCRIPTOR_NOTIFICATION);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    public void setRealtimeStepNotify() {
        mCharacteristic = getGattCharacteristicFromList(MiBandConstants.UUID_CHARACTERISTIC_REALTIME_STEPS);
        mBluetoothGatt.setCharacteristicNotification(mCharacteristic, true);
        BluetoothGattDescriptor descriptor = mCharacteristic.getDescriptor(MiBandConstants.UUID_DESCRIPTOR_NOTIFICATION);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    public void readRealtimeStep() {
        mCharacteristic = getGattCharacteristicFromList(MiBandConstants.UUID_CHARACTERISTIC_REALTIME_STEPS);
        mBluetoothGatt.readCharacteristic(mCharacteristic);
    }

    private void broadcastUpdate(final String action) {
        Log.i(TAG, "[ ] broadcast update: " + action);
        sendBroadcast(new Intent(action));
    }

    private void broadcastUpdate(final String action, BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);
        Log.i(TAG, "[ ] broadcast update with data: " + action);
        intent.putExtra(KEY_RAW_DATA, characteristic.getValue());
        intent.putExtra(KEY_UUID_STR, MiBandConstants.lookup(characteristic.getUuid(), MSG_KNOWN));
        sendBroadcast(intent);
    }

    private void close() {
        if(mBluetoothGatt == null)
            return;
        Log.i(TAG, "[ ] close...");
        mBluetoothGatt.disconnect();
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    private void buildServiceList(BluetoothGatt gatt) {
        Log.i(TAG, "[ ] build gatt service list");
        listGattService = gatt.getServices();
    }

    public BluetoothGattCharacteristic getGattCharacteristicFromList(UUID uuid) {
        if(listGattService == null)
            return null;
        for(BluetoothGattService service : listGattService) {
            for(BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                if(uuid.equals(characteristic.getUuid()))
                    return characteristic;
            }
        }
        return null;
    }

    public BluetoothGattService getGattServiceFromList(UUID uuid) {
        if(listGattService == null)
            return null;
        for(BluetoothGattService service : listGattService) {
            if(uuid.equals(service.getUuid()))
                return service;
        }
        return null;
    }

    public Object searchService(UUID uuid) {
        if(uuid == null)
            return null;
        for(BluetoothGattService service : listGattService) {
            if(uuid.equals(service.getUuid()))
                return service;

            for(BluetoothGattCharacteristic ch : service.getCharacteristics()) {
                if(uuid.equals(ch.getUuid()))
                    return ch;
            }
        }
        return null;
    }


    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "[ ] Service bind");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        Log.i(TAG, "[ ] Service unbind");
        return super.onUnbind(intent);
    }

}
