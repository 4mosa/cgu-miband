package tw.nagi.app.blehealth.util;

/**
 * String tools
 * @author Nagi
 */
public class StringUtils {


    /**
     * Check String is null or empty.
     *
     * @param str
     * @return True if str is null.
     */
    public static boolean isNullOrEmpty(String str) {
        if (str == null || str.length() == 0)
            return true;
        return false;
    }

    /**
     * Check the object is null or empty.
     *
     * @param obj
     * @return True if obj is null.
     */
    public static boolean isNullOrEmpty(Object obj) {
        if (obj == null || (obj instanceof String && ((String) obj).length() == 0))
            return true;
        return false;
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) {
            result.append(String.format("%02X ", b));
            result.append(" ");
        }
        return result.toString();
    }

    public static String bytesToHexWithoutSpace(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) {
            result.append(String.format("%02X", b));
        }
        return result.toString();
    }
}
