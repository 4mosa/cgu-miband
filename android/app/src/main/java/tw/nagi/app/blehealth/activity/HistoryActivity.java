package tw.nagi.app.blehealth.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import tw.nagi.app.blehealth.R;
import tw.nagi.app.blehealth.util.SharedPreferencesInterface;

/**
 * Created by nagilin on 15/09/23.
 */
public class HistoryActivity extends AppCompatActivity implements SharedPreferencesInterface {
    ArrayListAdapter mArrayListAdapter;
    private View vFinding, vDismiss;
    private static final String TAG = HistoryActivity.class.getSimpleName();
    private SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mArrayListAdapter = new ArrayListAdapter(this, android.R.layout.simple_list_item_2, new ArrayList<Map<String, String>>());
        ((ListView)findViewById(android.R.id.list)).setAdapter(mArrayListAdapter);

        vFinding = findViewById(R.id.linlaHeaderProgress);
        vDismiss = findViewById(R.id.scan_cantfind);

        sp = this.getSharedPreferences(SP_NAME, MODE_PRIVATE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vDismiss.setVisibility(View.GONE);
                        fade(vFinding, View.VISIBLE);
                    }
                });
                receive();

            }
        }).start();
    }

    private class ArrayListAdapter extends ArrayAdapter<Map<String, String>> {
        int mResource;
        public ArrayListAdapter(Context context, int resource, List<Map<String, String>> list) {
            super(context, resource, list);
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Map<String, String> item = getItem(position);
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(mResource, parent, false);
            }

            TextView t1 = (TextView) convertView.findViewById(android.R.id.text1);
            TextView t2 = (TextView) convertView.findViewById(android.R.id.text2);

            t1.setText(item.get("datetime"));
            t2.setText("跑了 " + item.get("realstep") + " 步");

            return convertView;
        }
    }



    private void receive() {
        HttpURLConnection conn = null;
        try {
            int id = sp.getInt(KEY_ID, 0);
            conn = (HttpURLConnection)new URL(getString(R.string.ADDR_SERV) + getString(R.string.ADDR_CTRL) + "?act=csv&p=m&uid=" + id).openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            String str = s.hasNext() ? s.next() : "";

            Log.i(TAG, str);

            JSONArray jarray = new JSONArray(str);

//            items = new ArrayList<Map<String, String>>();

            for(int i=0; i<jarray.length(); i++) {
                HashMap<String, String> item;
                JSONObject jobj = jarray.getJSONObject(i);
                item = new HashMap<String, String>();
                Iterator<String> iter = jobj.keys();
                while(iter.hasNext()) {
                    String key = iter.next();
                    item.put(key, jobj.getString(key));
                }

                final HashMap<String, String> puts = item;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mArrayListAdapter.add(puts);
                        mArrayListAdapter.notifyDataSetChanged();

                        if(mArrayListAdapter.getCount() == 0) {
                            vFinding.setVisibility(View.GONE);
                            vDismiss.setVisibility(View.VISIBLE);
                        } else {
                            vDismiss.setVisibility(View.GONE);
                            vFinding.setVisibility(View.GONE);
                        }
                    }
                });

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(conn != null)
                conn.disconnect();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void fade(final View view, final int viewStatus) {
        AlphaAnimation anime = new AlphaAnimation(((viewStatus == View.GONE) ? 1f : 0f), (viewStatus == View.GONE) ? 0f : 1f);
        anime.setDuration(500);
        view.startAnimation(anime);
        view.setVisibility(viewStatus);
    }
}
