package tw.nagi.app.blehealth.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;

import tw.nagi.app.blehealth.R;
import tw.nagi.app.blehealth.util.SharedPreferencesInterface;

/**
 * Created by nagilin on 2015/10/04.
 */
public class WelcomeActivity extends AppCompatActivity implements SharedPreferencesInterface {
    private static final String TAG = WelcomeActivity.class.getSimpleName();
    private Button btnLogin;
    private ProgressBar mProgressBar;
    private SharedPreferences sp;
    private Handler mHandler;
    private EditText txtUser, txtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        btnLogin = (Button) findViewById(R.id.btn_login);
        mProgressBar = (ProgressBar) findViewById(R.id.main_progressbar);
        txtUser = (EditText) findViewById(R.id.txtUser);
        txtPass = (EditText) findViewById(R.id.txtPass);


        sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);
        if(sp.contains(KEY_ID)) {
            startActivity(new Intent(WelcomeActivity.this, MiBandActivity.class));
            finish();
        }


        mHandler = new Handler();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.VISIBLE);
                    }
                });
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        login(txtUser.getText().toString(), txtPass.getText().toString());
                    }
                }).start();

            }
        });
    }
    public static String MD5(String str)
    {
        MessageDigest md5 = null;
        try
        {
            md5 = MessageDigest.getInstance("MD5");
        }catch(Exception e)
        {
            e.printStackTrace();
            return "";
        }

        char[] charArray = str.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for(int i = 0; i < charArray.length; i++)
        {
            byteArray[i] = (byte)charArray[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);

        StringBuffer hexValue = new StringBuffer();
        for( int i = 0; i < md5Bytes.length; i++)
        {
            int val = ((int)md5Bytes[i])&0xff;
            if(val < 16)
            {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }
    private void login(String user, String pass) {
        HttpURLConnection conn = null;


        try {
            pass = MD5(pass + "nagiisgod");

            conn = (HttpURLConnection)new URL(getString(R.string.ADDR_SERV) + getString(R.string.ADDR_CTRL) + "?act=login&user=" + user + "&pass=" + pass).openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            String str = s.hasNext() ? s.next() : "";
            final String msg = str;
            Log.d(TAG, "resp=" + str);
            if(str.contains("success")) {
                //success
                int id = Integer.parseInt(str.substring(7));
                sp.edit().putInt(KEY_ID, id).commit();
                startActivity(new Intent(WelcomeActivity.this, MiBandActivity.class));
                finish();
            } else {
                //fail
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(WelcomeActivity.this, "登入失敗！:" + msg, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        } catch(final Exception e) {
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(WelcomeActivity.this, "登入失敗！:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } finally {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(View.GONE);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
