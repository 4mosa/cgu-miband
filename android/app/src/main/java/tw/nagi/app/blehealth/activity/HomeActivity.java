package tw.nagi.app.blehealth.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;

import tw.nagi.app.blehealth.R;
import tw.nagi.app.blehealth.cview.CircleProgressBar;
import tw.nagi.app.blehealth.service.BluetoothLeService;
import tw.nagi.app.blehealth.util.MiBandConstants;
import tw.nagi.app.blehealth.util.SharedPreferencesInterface;
import tw.nagi.app.blehealth.util.StringUtils;


public class HomeActivity extends AppCompatActivity implements SharedPreferencesInterface {
    private static final String TAG = HomeActivity.class.getSimpleName();

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothLeService mBluetoothLeService;
    private BluetoothGatt mBluetoothGatt;
    private SharedPreferences mSharedPreferences;
    private Handler mHandler;
    private boolean mScanning;
    private String deviceAddr = "";
    private boolean isReconnect = false;
    private boolean isConnecting = false;
    private boolean isFound = false;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_BT_DEVICE = 2;
    private static final int BLUETOOTH_DELAY = 10000;    //10 sec
    private static final int ALPHA_DELAY = 500;    //1 sec
    private View vFinding, vDismiss, vMain;
    private TextView mProgressMsg, mUnderText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        vFinding = findViewById(R.id.main_layout_finding);
        vDismiss = findViewById(R.id.main_layout_dismiss);
        vMain = findViewById(R.id.main_layout_main);
        mProgressMsg = (TextView) findViewById(R.id.progress_message);
        mUnderText = (TextView) findViewById(R.id.main_progress_undertext);


        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            //do not have bt_le
            Toast.makeText(this, getString(R.string.DIALOG_MSG_NO_BLE), Toast.LENGTH_LONG).show();
            finish();
        }
        mSharedPreferences = getSharedPreferences(SP_NAME, MODE_PRIVATE);
        deviceAddr = mSharedPreferences.getString(KEY_DEVICE_ADDR, "");
        isReconnect = mSharedPreferences.getBoolean(KEY_AUTORECONN, false);


        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        mHandler = new Handler();



        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }


        if (deviceAddr != null && !deviceAddr.isEmpty()) {
            scanLeDevice(true);
        } else {
            show(vDismiss);
        }


        findViewById(R.id.main_progressbar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mBluetoothGatt != null && isConnecting) {
                    vibrate();
                    readRealstep();
                }
                return true;
            }
        });

    }

    private void pair() {
        if (mBluetoothGatt == null) {
            Log.w(TAG, "pair(): gatt is null");
            return; //Do nothing.
        }

        BluetoothGattCharacteristic chrt = mBluetoothGatt.getService(MiBandConstants.UUID_SERVICE_MIBAND_SERVICE).getCharacteristic(MiBandConstants.UUID_CHARACTERISTIC_PAIR);
        chrt.setValue(new byte[]{2});
        mBluetoothGatt.writeCharacteristic(chrt);
    }

    private void vibrate() {
        if (mBluetoothGatt == null) {
            Log.w(TAG, "vibrate(): gatt is null");
            return; //Do nothing.
        }

        BluetoothGattCharacteristic chrt = mBluetoothGatt.getService(MiBandConstants.UUID_SERVICE_IAS).getCharacteristic(MiBandConstants.UUID_CHARACTERISTIC_IAS);
        chrt.setValue(new byte[]{1});
        mBluetoothGatt.writeCharacteristic(chrt);
    }

    private void readRealstep() {
        if (mBluetoothGatt == null) {
            Log.w(TAG, "readRealstep(): gatt is null");
            return; //Do nothing.
        }

        mBluetoothGatt.readCharacteristic(mBluetoothGatt.getService(MiBandConstants.UUID_SERVICE_MIBAND_SERVICE).getCharacteristic(MiBandConstants.UUID_CHARACTERISTIC_REALTIME_STEPS));
    }

    private void setAllLayoutGone(boolean fade) {
        if (fade) {
            fade(vDismiss, View.GONE);
            fade(vMain, View.GONE);
            fade(vDismiss, View.GONE);
        } else {
            vDismiss.setVisibility(View.GONE);
            vMain.setVisibility(View.GONE);
            vFinding.setVisibility(View.GONE);
        }
    }

    public void show(final View view) {
        show(view, false);
    }

    public void show(final View view, boolean fade) {
        setAllLayoutGone(fade);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fade(view, View.VISIBLE);
            }
        }, ALPHA_DELAY);
    }

    public void fade(final View view, final int viewStatus) {
        AlphaAnimation anime = new AlphaAnimation(((viewStatus == View.GONE) ? 1f : 0f), (viewStatus == View.GONE) ? 0f : 1f);
        anime.setDuration(ALPHA_DELAY);
        view.startAnimation(anime);
        view.setVisibility(viewStatus);
    }

    public void callDevScanActivity() {
        Intent i = new Intent(HomeActivity.this, DeviceListActivity.class);
        if (!deviceAddr.isEmpty()) i.putExtra(KEY_DEV_ACTIVITY, deviceAddr);
        startActivityForResult(i, REQUEST_BT_DEVICE);
    }

//    public void registerActionFilter(String action) {
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(action);
//        registerReceiver(mReceiver, filter);
//    }


    public void scanLeDevice(final boolean enable) {
        if (enable) {
            //設定BLUETOOTH_DELAY毫秒後停止搜尋
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    show((isFound) ? vMain : vDismiss);
                    mBluetoothGatt = mBluetoothDevice.connectGatt(HomeActivity.this, false, mGattCallBack);
                }
            }, BLUETOOTH_DELAY);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            show((isFound) ? vMain : vDismiss);
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (deviceAddr.equals(device.getAddress())) {
                        mBluetoothDevice = device;
                        isFound = true;
                    }
                }
            });
        }
    };

    private BluetoothGattCallback mGattCallBack = new BluetoothGattCallback() {
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "[+] onCharacteristicRead");
            byte b[] = characteristic.getValue();
            if (characteristic.getUuid().equals(MiBandConstants.UUID_CHARACTERISTIC_REALTIME_STEPS)) {
                int step = (0xff & b[0] | (0xff & b[1]) << 8);
                ((CircleProgressBar) findViewById(R.id.main_progressbar)).setProgressNotInUiThread(step);
                readRealstep();
            }

        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i(TAG, "[+] onConnectionStateChange( GattSuccess=" + (status == BluetoothGatt.GATT_SUCCESS) + " ,GattConn=" + (newState == BluetoothGatt.STATE_CONNECTED) + " )");
            if (newState == BluetoothProfile.STATE_CONNECTED && status == BluetoothGatt.GATT_SUCCESS) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressMsg.setText(getText(R.string.TEXT_DEV_CONNECTED));
                        show(vMain);
                    }
                });
                mBluetoothGatt.discoverServices();

            } else {
                isConnecting = false;
                if (isReconnect) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressMsg.setText(getText(R.string.TEXT_NOW_SYNC));
                            show(vFinding);
                        }
                    });
                    mBluetoothGatt.connect();
                } else {
                    mBluetoothGatt.disconnect();
                    mBluetoothGatt.close();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mUnderText.setText(getText(R.string.TEXT_DISCONNECT));
                        }
                    });

                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            isConnecting = true;
            Log.i(TAG, "[+] onServicesDiscovered");
            vibrate();

            pair();
            Log.i(TAG, "[+] pair()");
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "[+] onCharacteristicWrite(" + status + "):" + StringUtils.bytesToHex(characteristic.getValue()));
            vibrate();
            readRealstep();
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem mi = menu.findItem(R.id.action_autoreconn);
        mi.setIcon((isReconnect) ? R.drawable.ic_sync : R.drawable.ic_sync_disable);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case R.id.action_confirm:
                callDevScanActivity();
                return true;
            case R.id.action_autoreconn:
                if (isReconnect) {
                    isReconnect = false;
                    item.setIcon(R.drawable.ic_sync_disable);
                    mSharedPreferences.edit().putBoolean(KEY_AUTORECONN, false).commit();
                } else {
                    isReconnect = true;
                    item.setIcon(R.drawable.ic_sync);
                    mSharedPreferences.edit().putBoolean(KEY_AUTORECONN, true).commit();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_BT_DEVICE:
                if (REQ_OK == resultCode) {
                    mBluetoothDevice = data.getParcelableExtra(KEY_DEV_ACTIVITY);
                    isFound = true;
                    show(vMain);
                    mBluetoothGatt = mBluetoothDevice.connectGatt(HomeActivity.this, false, mGattCallBack);
                }
                break;
        }
    }
}
