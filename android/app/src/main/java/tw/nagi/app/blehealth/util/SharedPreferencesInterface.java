package tw.nagi.app.blehealth.util;

/**
 * Created by nagilin on 15/09/07.
 */
public interface SharedPreferencesInterface {

    String KEY_INIT = "init";
    String KEY_DEVICE_ADDR = "address";
    String KEY_RAW_DATA = "rawdata";
    String KEY_UUID_STR = "uuidstr";
    String MSG_KNOWN = "unknown";
    String KEY_DEV_ACTIVITY = "devicenow";
    String KEY_REALSTEP = "realstep";
    int REQ_OK = 1;
    String SP_NAME = "sharedpreferencename";
    String KEY_AUTORECONN = "reconnect";
    String KEY_ID = "userid";
    String KEY_UPLOADING = "uploadkey";
}
