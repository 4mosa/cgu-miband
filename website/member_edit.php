<?php
  session_start();
  include 'config.php';
  $title = "管理會員";
  if(!isset($_SESSION['user_session']) || $_SESSION['user_session']['permission'] <= 0) {   
    die('請先登入管理員');
  }
  if(isset($_POST['account'])) {
    //update
    $db = new PDO("mysql:host=localhost;dbname=ble", 'ble', 'bleelb');
    $sql = "update member set account = '" . $_POST['account'] . "', permission = '" . $_POST['permission'] . "' ";
    if(!empty($_POST['password'])) {
      $sql .= ", password = '" . md5($_POST['password'].SALT) ."'";
    }
    $sql .= ' where id=?';
    $stmt = $db->prepare($sql); 
    $success = $stmt->execute(array($_POST['id']));
    if($success) {
      header('Location: member_mgt.php');
      exit();
    } else {
      echo '<script>alert("修改失敗");</script>';
      
    }

    
  }

  if(!isset($_GET['eid'])) {
    die('參數錯誤');
  }
  $uid = $_GET['eid'];

  $db = new PDO("mysql:host=localhost;dbname=ble", 'ble', 'bleelb');
  $stmt = $db->prepare("select * from member where id=?"); 
  $stmt->execute(array($uid));
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<html>
<head>
  <meta charset="utf-8">
  <title><?=WEB_TITLE?> - <?=$title?></title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <style type="text/css">
    .dropdown .dropdown-menu {
      margin-bottom: 20px;
    }

    .body {
      margin-top: 70px;
    }

    button {
      margin: 5px;
    }

  </style>
</head>
<body>
  <?php include 'nav.php'; ?>

  <div class="col-md-offset-2 col-md-8 body" role="main">
    <div class="page-header">
      <h1>編輯會員</h1>
    </div>
    <form method="POST" name="form">
      <input type="hidden" name="id" value="<?=$row['id']?>">
      <div class="form-group">
        <label>編號</label>
        <input type="text" class="form-control" id="uid" name="uid" value="<?=$row['id']?>" disabled> 
      </div>
      <div class="form-group">
        <label>帳號</label>
        <input type="text" class="form-control" id="account" name="account" value="<?=$row['account']?>"> 
      </div>
      <div class="form-group">
        <label>新密碼（若為空白則不變動）</label>
        <input type="password" class="form-control" id="password" name="password" value=""> 
      </div>
      <div class="form-group">
        <label>權限</label>
        <select class="form-control" id="permission" name="permission">
          <option value="0" <?php if($row['permission'] == '0') echo 'selected';?>>普通</option>
          <option value="1" <?php if($row['permission'] == '1') echo 'selected';?>>管理員</option>
        </select>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-success">更新</button>
        <button type="button" class="btn btn-danger" onclick="window.location.replace('member_mgt.php');">取消</button>
      </div>
    </form>
  </div>

  



  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
  <script src="js/jquery.jeditable.mini.js" type="text/javascript"></script>
</body>
</html>