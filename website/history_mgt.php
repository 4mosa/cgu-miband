<?php
  session_start();
  include 'config.php';
  $title = "管理歷史紀錄";

  if(!isset($_SESSION['user_session']) || $_SESSION['user_session']['permission'] <= 0) {   
    die('請先登入管理員');
  }

  $dsn = "mysql:host=localhost;dbname=ble";
  $db = new PDO($dsn, 'ble', 'bleelb');
  $stmt = $db->prepare("select * from member");
  $stmt->execute();
  $members = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>



<html>
<head>
  <meta charset="utf-8">
  <title><?=WEB_TITLE?> - <?=$title?></title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <style type="text/css">
    .dropdown .dropdown-menu {
      margin-bottom: 20px;
    }

    .body {
      margin-top: 70px;
    }

  </style>
</head>
<body>
  
  <?php include 'nav.php'; ?>

  <div class="col-md-offset-1 col-md-10 body" role="main">
    <div class="page-header">
      <h1>歷史紀錄管理</h1>
        <div class="form-inline form-group">
          <select class="form-control" id="member_picker">
            <option value="0" selected></option>
          <?php
            foreach($members as $member) {
              echo '<option value="'.$member['id'].'">'.$member['account'].'</option>';
            }
          ?>
          </select>
        </div>
        <ul class="nav nav-pills" id="tabnav">
          <li role="presestation" class="active"><a href="#realstep">步數記錄</a></li>
          <li role="presestation"><a href="#sleep">睡眠記錄</a></li>
        </ul>

    </div>
    <div class="tab-content"> <!-- TAB content -->
      <div role="tabpanel" class="tab-pane fade in active" id="realstep">
        <!-- <button class="btn btn-primary" id="btn_csv"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>下載csv</button> -->
        <table class="table table-hover" id="history_table">
          <thead>
            <tr>
              <th><a htrf="#">#</a></th>
              <th><a htrf="#">步數</a></th>
              <!-- <th><a htrf="#">睡眠訊息</a></th> -->
              <th><a htrf="#">記錄時間</a></th>
            </tr>
          </thead>
          <tbody id="tbody_step">
            
          </tbody>
        </table>
      </div> <!-- close tab panel -->
      <div role="tabpanel" class="tab-pane fade" id="sleep"> <!-- tab panel -->
        <!-- <button class="btn btn-primary" id="btn_sleep_csv"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>下載csv</button> -->
        <table class="table table-hover" id="sleep_table">
          <thead>
            <tr>
              <th><a htrf="#">#</a></th>
              <th><a htrf="#">睡眠時長</a></th>
              <th><a htrf="#">深睡時長</a></th>
              <th><a htrf="#">淺睡時長</a></th>
              <th><a htrf="#">入睡時間</a></th>
              <th><a htrf="#">醒來時間</a></th>
              <th><a htrf="#">清醒時長</a></th>
            </tr>
          </thead>
          <tbody id="tbody_sleep">
            
          </tbody>
        </table>
      </div>
    </div>

  </div>


  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
  <script src="js/history_mgt.js" type="text/javascript"></script>
</body>
</html>