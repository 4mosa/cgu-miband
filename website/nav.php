  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand title" href="#" style="font-size: x-large;"><?=WEB_TITLE?></a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <div class="navbar-right">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="profile_dropdown"><?=$_SESSION['user_session']['account']?><span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="profile_dropdown">
                <li><a href="history.php">歷史紀錄</a></li>
                <?php if($_SESSION['user_session']['permission'] > 0) echo '<li><a href="member_mgt.php">管理會員</a></li>'; ?>
                <?php if($_SESSION['user_session']['permission'] > 0) echo '<li><a href="history_mgt.php">管理歷史紀錄</a></li>'; ?>
                <li><a href="javascript:logout();">登出</a></li>
              </ul>
            </li>
          </ul>
        </div>
      <div>
    </div>
  </nav>