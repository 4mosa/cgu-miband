<?php
  session_start();
  include 'config.php';
  require_once 'session.php';
  $title = "登入";
  $no_account = 'hidden';
  $wrong_pass = 'hidden';

  if(isset($_POST['account']) && isset($_POST['password'])) {  //login
    $db = new PDO("mysql:host=localhost;dbname=ble", 'ble', 'bleelb');
    $stmt = $db->prepare("select * from member where account = ?");
    @$stmt->execute(array($_POST['account']));
    $accounts = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if(sizeof($accounts) == 0) {
      //no this account
      $no_account = 'visible';
    } else {
      $account = $accounts[0];
      if(md5($_POST['password'].SALT) == $account['password']) {
        //login
        $_SESSION['user_session'] = $account;
        header('Location: history.php'); 
        exit();
      } else {
        //login fail
        $wrong_pass = 'visible';
      }
    }



  } 
?>

<html>
<head>
  <meta charset="utf-8">
  <title><?=WEB_TITLE?> - <?=$title?></title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <style type="text/css">
    div {
      text-align: center;
      padding: 5px;
    }

    .login_form {
      margin: 20px;
      opacity: 0.7;
      display: inline-block;
    }
  </style>


</head>
<body>
  <form class="form-group form-inline col-md-offset-4" name="login" method="POST">
    <div class="login_form well">
      <div>
        帳號：
        <input type="text" name="account" class="form-control" value="<?php if(isset($_POST['account'])) echo $_POST['account']; ?>">
        <div style="color: red; visibility: <?=$no_account?>;">找不到帳號</div>
      </div>
      <div>
        密碼：
        <input type="password" name="password" class="form-control" value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>">
        <div style="color: red; visibility: <?=$wrong_pass?>;">密碼錯誤</div> 
      </div>
      <div>
        <button name="btn_login" class="btn btn-primary" type="submit">登入</button>
      </div>
    </div>  
  </form>

  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/front.js" type="text/javascript"></script>
</body>
</html>