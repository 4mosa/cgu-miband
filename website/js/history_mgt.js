$(document).ready(function() {
  $("#member_picker").change(function(e) {
    $("#tbody_step").html('');
    var id = $("#member_picker option:selected").val();
    $.ajax({
      url: 'ctrl.php?act=his&qid=' + id + '&mo=step',
      success: function(a) {
        $("#tbody_step").append(a).hide().fadeIn(500);
      }
    });
    $("#tbody_sleep").html('');
    $.ajax({
      url: 'ctrl.php?act=his&qid=' + id + '&mo=sleep',
      success: function(a) {
        $("#tbody_sleep").append(a).hide().fadeIn(500);
      }
    });
    
    $("#history_table").tablesorter();
    $("#sleep_table").tablesorter();
  });

  $('#tabnav a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
});