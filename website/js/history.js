$(document).ready(function() {
  $("#history_table").tablesorter();
  $("#sleep_table").tablesorter();
  
  $("#btn_csv").click(function() {
      window.open("ctrl.php?act=csv");
  });

  $('#tabnav a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
});


function logout() {
  $.ajax({
    url: "ctrl.php?act=logout",
    success: function() {
      window.location.replace("index.php");
    }
  });
}