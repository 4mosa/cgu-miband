var edit_call = function() {
    var id = $(this).parent().parent().attr('id').substring(3);
    window.location.replace("member_edit.php?eid=" + id);
  };

var delete_call = function() {
    var id = $(this).parent().parent().attr('id').substring(3);
    var btn = $(this);
    $.ajax({
      url: "ctrl.php?act=me&do=d&id=" + id,
      success: function() {
        btn.parent().parent().fadeOut(500);
      }
    });
  };

$(document).ready(function() {
  $(".edit").click(edit_call);

  $(".delete").click(delete_call);

  $(".add").click(function() {
    $.ajax({
      url: "ctrl.php?act=me&do=a",
      method: "POST",
      data: $("#newmember").serialize(),
      success: function(data) {
        data = JSON.parse(data);
        var html = "<tr id=\"row" + data['id'] + "\"><td>" + data['id'] + "</td><td>" + data['account'] + "</td><td>" + data['password'] + "</td><td>" + ((data['permission'] == 0)?'普通':'管理員') + "</td><td><button type=\"button\" class=\"btn btn-warning edit\">修改</button><button type=\"button\" class=\"btn btn-danger delete\">刪除</button></td></tr>";
        $("#newmember").before(html).fadeIn(500);
        $(".edit").click(edit_call);
        $(".delete").click(delete_call);
      }
    });
    
  });
});



