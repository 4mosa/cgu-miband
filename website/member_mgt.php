<?php
  session_start();
  include 'config.php';
  $title = "管理會員";
  if(!isset($_SESSION['user_session']) || $_SESSION['user_session']['permission'] <= 0) {   
    die('請先登入管理員');
  }

  $db = new PDO("mysql:host=localhost;dbname=ble", 'ble', 'bleelb');
  $stmt = $db->prepare("select * from member"); 
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>

<html>
<head>
  <meta charset="utf-8">
  <title><?=WEB_TITLE?> - <?=$title?></title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <style type="text/css">
    .dropdown .dropdown-menu {
      margin-bottom: 20px;
    }

    .body {
      margin-top: 70px;
    }

    button {
      margin: 5px;
    }

  </style>
</head>
<body>
  <?php include 'nav.php'; ?>

  <div class="col-md-offset-2 col-md-8 body" role="main">
    <div class="page-header">
      <h1>管理會員</h1>
    </div>
    
    <table class="table table-hover">
      <thead>
        <tr>
          <th>#</th>
          <th>帳號名稱</th>
          <th>密碼(加密)</th>
          <th>權限</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody id="tbody">
        <?php
          foreach ($result as $row) {
            echo '<tr id="row'.$row['id'].'">';
            echo '<td>'.$row['id'].'</td>';
            echo '<td>'.$row['account'].'</td>';
            echo '<td>'.$row['password'].'</td>';
            echo '<td>'.(($row['permission'] == 0)?'普通':'管理員').'</td>';
            echo '<td><button type="button" class="btn btn-warning edit">修改</button>
                  <button type="button" class="btn btn-danger delete">刪除</button></td>';
            echo '</tr>';
          }
        ?>
        <form id="newmember">
          <tr class="form-inline form-group">
            <td></td>
            <td><input type="text" name="account" class="form-control"></td>
            <td><input type="password" name="password" class="form-control"></td>
            <td>
              <select name="permission" class="form-control">
                <option value="0">普通</option>
                <option value="1">管理員</option>
              </select>
            </td>
            <td><button type="button" class="btn btn-success add">新增</button></td>
          </tr>
        </form>
      </tbody>
    </table>
  </div>

  



  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
  <script src="js/jquery.jeditable.mini.js" type="text/javascript"></script>
  <script src="js/mgt.js" type="text/javascript"></script>
</body>
</html>