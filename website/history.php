<?php
  session_start();
  include 'config.php';
  require_once 'session.php';
  $title = "歷史紀錄";
  if(!isset($_SESSION['user_session'])) {
    header('Location: index.php');
    exit();
  }


  $dsn = "mysql:host=localhost;dbname=ble";
  $db = new PDO($dsn, 'ble', 'bleelb');
  $stmt = $db->prepare("select * from user_data where uid = ? order by id");
  $stmt->execute(array($_SESSION['user_session']['id']));
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

  $stmts = $db->prepare("select * from sleep_data where uid = ? order by id");
  $stmts->execute(array($_SESSION['user_session']['id']));
  $sleep_result = $stmts->fetchAll(PDO::FETCH_ASSOC);

?>

<html>
<head>
  <meta charset="utf-8">
  <title><?=WEB_TITLE?> - <?=$title?></title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <style type="text/css">
    .dropdown .dropdown-menu {
      margin-bottom: 20px;
    }

    .body {
      margin-top: 70px;
    }

  </style>
</head>
<body>
  
  <?php include 'nav.php'; ?>

  <div class="col-md-offset-1 col-md-10 body" role="main">
    <div class="page-header">
      <h1>歷史紀錄</h1>
      
        <ul class="nav nav-pills" id="tabnav">
          <li role="presestation" class="active"><a href="#realstep">步數記錄</a></li>
          <li role="presestation"><a href="#sleep">睡眠記錄</a></li>
        </ul>

    </div>
    <div class="tab-content"> <!-- TAB content -->
      <div role="tabpanel" class="tab-pane fade in active" id="realstep">
        <button class="btn btn-primary" id="btn_csv"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>下載csv</button>
        <table class="table table-hover" id="history_table">
          <thead>
            <tr>
              <th><a htrf="#">#</a></th>
              <th><a htrf="#">步數</a></th>
              <!-- <th><a htrf="#">睡眠訊息</a></th> -->
              <th><a htrf="#">記錄時間</a></th>
            </tr>
          </thead>
          <tbody id="tbody">
            <?php
              foreach ($result as $row) {
                  echo '<tr>';
                  echo '<td>'.$row['id'].'</td>';
                  echo '<td>'.$row['realstep'].'</td>';
                  // echo '<td>'.'0'.'</td>';
                  echo '<td>'.$row['datetime'].'</td>';
                  echo '</tr>';
              }
            ?>
          </tbody>
        </table>
      </div> <!-- close tab panel -->
      <div role="tabpanel" class="tab-pane fade" id="sleep"> <!-- tab panel -->
        <!-- <button class="btn btn-primary" id="btn_sleep_csv"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>下載csv</button> -->
        <table class="table table-hover" id="sleep_table">
          <thead>
            <tr>
              <th><a htrf="#">#</a></th>
              <th><a htrf="#">睡眠時長</a></th>
              <th><a htrf="#">深睡時長</a></th>
              <th><a htrf="#">淺睡時長</a></th>
              <th><a htrf="#">入睡時間</a></th>
              <th><a htrf="#">醒來時間</a></th>
              <th><a htrf="#">清醒時長</a></th>
            </tr>
          </thead>
          <tbody id="tbody">
            <?php
              foreach ($sleep_result as $row) {
                  //process each data to currect format

                  $row['sleep_len'] = WebUtils::ms2TimeLen($row['sleep_len']);
                  $row['deep_sleep_len'] = WebUtils::ms2TimeLen($row['deep_sleep_len']);
                  $row['light_sleep_len'] = WebUtils::ms2TimeLen($row['light_sleep_len']);
                  $row['wakeup_len'] = WebUtils::ms2TimeLen($row['wakeup_len']);
                  $row['sleep_time'] = WebUtils::dtstr2Time($row['sleep_time']);
                  $row['wakeup_time'] = WebUtils::dtstr2Time($row['wakeup_time']);

                  echo '<tr>';
                  echo '<td>'.$row['id'].'</td>';
                  echo '<td>'.$row['sleep_len'].'</td>';
                  echo '<td>'.$row['deep_sleep_len'].'</td>';
                  echo '<td>'.$row['light_sleep_len'].'</td>';
                  echo '<td>'.$row['sleep_time'].'</td>';
                  echo '<td>'.$row['wakeup_time'].'</td>';
                  echo '<td>'.$row['wakeup_len'].'</td>';
                  echo '</tr>';
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>

  </div>


  <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
  <script src="js/history.js" type="text/javascript"></script>
</body>
</html>