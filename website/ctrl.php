<?php
  session_start();
  include 'config.php';
  require_once 'session.php';


  if(isset($_GET['act'])) {
    if($_GET['act'] == 'login') {
      //mobile login
      $user = $_GET['user'];
      $pass = $_GET['pass'];

      $db = new PDO("mysql:host=localhost;dbname=ble", 'ble', 'bleelb');
      $stmt = $db->prepare("select * from member where account = ?");
      @$stmt->execute(array($user));
      $accounts = $stmt->fetchAll(PDO::FETCH_ASSOC);

      if(sizeof($accounts) == 0) {
        echo 'no_account';
      } else {
        $account = $accounts[0];
        if($pass == $account['password']) {
          //login
          echo 'success'.$account['id'];
        } else {
          echo 'wrong_pass';
        }
      }
    }
    else if($_GET['act'] == 'logout') {
      //logout
      unset($_SESSION['user_session']);
    }
    else if($_GET['act'] == 'csv') {
      $uid = 0;
      if(isset($_GET['p']) && $_GET['p'] == 'm') {
        $uid = $_GET['uid'];
      } else {
        $uid = @$_SESSION['user_session']['id'];  
      }
      $dsn = "mysql:host=localhost;dbname=ble";
      $db = new PDO($dsn, 'ble', 'bleelb');
      $stmt = $db->prepare("describe user_data");
      $stmt->execute();
      $fields = $stmt->fetchAll(PDO::FETCH_COLUMN);
      $stmt = $db->prepare("select * from user_data where uid = ?");
      $stmt->execute(array($uid));
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if(isset($_GET['p']) && $_GET['p'] == 'm') {
        echo json_encode($result);
      } else {
        $filename = 'tmp/'.date('YmdHisu').'.csv';
        $fp = fopen($filename, 'w');
        fputcsv($fp, $fields);
        foreach($result as $row) {
          fputcsv($fp, $row);
        }
        fclose($fp);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($filename));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit;        
      }


    } 
    else if($_GET['act'] == 'test') {
      echo WebUtils::genRandStr();
    }
    else if($_GET['act'] == 'me') {
      $dsn = "mysql:host=localhost;dbname=ble";
      $db = new PDO($dsn, 'ble', 'bleelb');
      if($_GET['do'] == 'd') {
        $id = $_GET['id'];
        $stmt = $db->prepare("delete from member where id=?");
        $stmt->execute(array($id));
      }
      else if($_GET['do'] == 'a') {
        $user = $_POST['account'];
        $pass = md5($_POST['password'].SALT);
        $permission = $_POST['permission'];       
        $stmt = $db->prepare("insert into member (account, password, permission) values (?,?,?)");
        
        
        
        $success = $stmt->execute(array($user, $pass, $permission));
        if($success) {
          $stmts = $db->prepare("select id from member where account = ? and password = ? and permission = ?");
          $stmts->execute(array($user, $pass, $permission));
          $id = @$stmts->fetch(PDO::FETCH_ASSOC)['id'];
          echo json_encode(array('id'=>$id, 'account'=>$user, 'password'=>$pass, 'permission'=>$permission));
        } else {
          http_response_code(999);
        }
      }
    }
    else if($_GET['act'] == 'his') {
      $dsn = "mysql:host=localhost;dbname=ble";
      $db = new PDO($dsn, 'ble', 'bleelb');

      $uid = @$_GET['uid'];
      $qid = @$_GET['qid'];

      if($_GET['mo'] == 'step') {
        $stmt = $db->prepare("select * from user_data where uid = ? order by id");
        $stmt->execute(array($qid));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $res = '';
        foreach ($result as $row) {
                  $res .= '<tr>';
                  $res .= '<td>'.$row['id'].'</td>';
                  $res .= '<td>'.$row['realstep'].'</td>';
                  $res .= '<td>'.$row['datetime'].'</td>';
                  $res .= '</tr>';
              }
        echo $res; 
      } else if($_GET['mo'] == 'sleep'){
        $stmts = $db->prepare("select * from sleep_data where uid = ? order by id");
        $stmts->execute(array($qid));
        $sleep_result = $stmts->fetchAll(PDO::FETCH_ASSOC);

        $res = '';
        foreach ($sleep_result as $row) {
                  //process each data to currect format

                  $row['sleep_len'] = WebUtils::ms2TimeLen($row['sleep_len']);
                  $row['deep_sleep_len'] = WebUtils::ms2TimeLen($row['deep_sleep_len']);
                  $row['light_sleep_len'] = WebUtils::ms2TimeLen($row['light_sleep_len']);
                  $row['wakeup_len'] = WebUtils::ms2TimeLen($row['wakeup_len']);
                  $row['sleep_time'] = WebUtils::dtstr2Time($row['sleep_time']);
                  $row['wakeup_time'] = WebUtils::dtstr2Time($row['wakeup_time']);

                  $res .= '<tr>';
                  $res .= '<td>'.$row['id'].'</td>';
                  $res .= '<td>'.$row['sleep_len'].'</td>';
                  $res .= '<td>'.$row['deep_sleep_len'].'</td>';
                  $res .= '<td>'.$row['light_sleep_len'].'</td>';
                  $res .= '<td>'.$row['sleep_time'].'</td>';
                  $res .= '<td>'.$row['wakeup_time'].'</td>';
                  $res .= '<td>'.$row['wakeup_len'].'</td>';
                  $res .= '</tr>';
              }
        echo $res;
      }
      

      
    }

  } else {
    http_response_code(999);
  }




?>