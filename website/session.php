<?php
  class SessionUtils {
    public static function check_session($key) {
      $dsn = "mysql:host=localhost;dbname=ble";
      $db = new PDO($dsn, 'ble', 'bleelb');

      $stmt = $db->prepare("select session.*, member.account, member.permission from session join member on member.id = session.uid where skey = ?");
      $stmt->execute(array($key));

      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      return $result;
    }

    public static function insert_session($key, $uid) {
      $dsn = "mysql:host=localhost;dbname=ble";
      $db = new PDO($dsn, 'ble', 'bleelb');

      $stmt = $db->prepare("insert into session (skey, uid) values (?, ?)");
      try {
        $stmt->execute(array($key, $uid));
        return true;
      } catch(Exception $e) {
        return false; //error handler
      }
    }

    public static function array_to_csv($fields, $delimiter = ',', $enclosure = '"')
    {
        $csv = '';

        foreach ($fields as $field) {
            $first_element = true;

            foreach ($field as $element) {
                // 除了第一個欄位外, 於 每個欄位 前面都需加上 欄位分隔符號
                if (!$first_element)
                   $csv .= $delimiter;

                $first_element = false;

                // CSV 遇到 $enclosure, 需要重複一次, ex: " => ""
                $element = str_replace($enclosure, $enclosure . $enclosure, $element);
                $csv .= $enclosure . $element . $enclosure;
            }

            $csv .= "\n";
        }

        return $csv;
    }
  }

  class WebUtils {
    var $dsn = "mysql:host=localhost;dbname=ble";

    public static function genRandStr($len = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
      return substr(str_shuffle($characters), 0, $len);
    }

    public static function ms2TimeLen($secs) {
      if(empty($secs) || $secs == null)
        return '';

      $bit = array(
        ' 年'        => $secs / 31556926 % 12,
        ' 周'        => $secs / 604800 % 52,
        ' 日'        => $secs / 86400 % 7,
        ' 小時'        => $secs / 3600 % 24,
        ' 分鐘'    => $secs / 60 % 60,
        ' 秒'    => $secs % 60
        );
        
      foreach($bit as $k => $v){
          // if($v > 1)$ret[] = $v . $k . 's';
          // if($v == 1)$ret[] = $v . $k;
          if($v >= 1) $ret[] = $v.$k;
          }
      // array_splice($ret, count($ret)-1, 0, 'and');
      // $ret[] = 'ago.';
      
      return join(' ', $ret);
    }

    public static function dtstr2Time($dt) {
      if(empty($dt) || $dt == null)
        return '';
      return date('H:i', strtotime($dt));
    }

  }


  // echo WebUtils::ms2TimeLen(10000);
  // echo WebUtils::dtstr2Time('2015-10-01 10:00:00');
?>